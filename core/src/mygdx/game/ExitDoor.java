package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class ExitDoor extends StaticObject {
    public ExitDoor(float width, float height, Vector2 pos, float angle, ArrayList<Texture> anim, World world) {
        super(width, height, pos, angle, anim, world);
        body = createPolygonBody();
        body.setActive(false);
    }
    boolean hasMeet(Vector2 pos) {
        if(pos.x > getPos().x && pos.x < getPos().x + width && pos.y > getPos().y && pos.y < getPos().y + height) {
            return true;
        }
        return false;
    }
}
