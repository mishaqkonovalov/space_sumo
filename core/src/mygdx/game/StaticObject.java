package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class StaticObject extends GameObject {
    float angle;

    public StaticObject(float width, float height, Vector2 pos, float angle, ArrayList<Texture> anim, World world) {
        super(width, height, pos, anim, world);
        this.angle = angle;
        bodyDef.type = BodyDef.BodyType.StaticBody;
        density = 0;
    }
}
