package mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class MainMenuScreen implements Screen {
    public static final int ANDROID_HEIGHT = Gdx.graphics.getHeight();
    public int ANDROID_WIDTH = Gdx.graphics.getWidth();

    SpriteBatch batch;
    Vector2 touch;

    Button buttonContinue;
    Button buttonStart;
    Button buttonOptions;

    float buttonSize = 0.18f;
    float buttonIndent = 0.05f;
    float buttonYPos = 0.6f;

    Texture buttonContinueTexture;
    Texture buttonStartTexture;
    Texture buttonOptionsTexture;

    boolean switchToGame = false;

    MainMenuScreen(Game game) {
        batch = new SpriteBatch();
        touch = new Vector2();

        buttonContinueTexture = new Texture("Main_Menu/Continue.png");
        buttonStartTexture = new Texture("Main_Menu/Start.png");
        buttonOptionsTexture = new Texture("Main_Menu/Options.png");

        buttonContinue = new Button((1 - buttonSize) / 2 * ANDROID_WIDTH, buttonYPos * ANDROID_HEIGHT, buttonSize, buttonContinueTexture);
        buttonStart = new Button((1 - buttonSize) / 2 * ANDROID_WIDTH, buttonContinue.screenY - (buttonSize + buttonIndent) * ANDROID_HEIGHT, buttonSize, buttonStartTexture);
        buttonOptions = new Button((1 - buttonSize) / 2 * ANDROID_WIDTH, buttonStart.screenY - (buttonSize + buttonIndent) * ANDROID_HEIGHT, buttonSize, buttonOptionsTexture);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        if(Gdx.input.isTouched()) {
            touch.set(new Vector2(Gdx.input.getX(), ANDROID_HEIGHT - Gdx.input.getY()));

            if(touch.x > buttonContinue.screenX && touch.y < buttonContinue.screenX + buttonContinue.size * ANDROID_WIDTH && touch.y > buttonContinue.screenY && touch.y < buttonContinue.screenY + buttonContinue.size * ANDROID_HEIGHT) {
                buttonContinue.isTouched = true;
            }
            if(touch.x > buttonStart.screenX && touch.y < buttonStart.screenX + buttonStart.size * ANDROID_WIDTH && touch.y > buttonStart.screenY && touch.y < buttonStart.screenY + buttonStart.size * ANDROID_HEIGHT) {
                buttonStart.isTouched = true;
            }
            if(touch.x > buttonOptions.screenX && touch.y < buttonOptions.screenX + buttonOptions.size * ANDROID_WIDTH && touch.y > buttonOptions.screenY && touch.y < buttonOptions.screenY + buttonOptions.size * ANDROID_HEIGHT) {
                buttonOptions.isTouched = true;
            }
        }

        if(buttonContinue.isTouched) {
            switchToGame = true;
        }
        if(buttonStart.isTouched) {
            switchToGame = true;
        }

        System.out.println("Main");
        batch.begin();
        batch.draw(buttonContinue.texture, buttonContinue.screenX, buttonContinue.screenY, buttonContinue.size * ANDROID_WIDTH, buttonContinue.size * ANDROID_HEIGHT, 0, 0, 256, 64, false, false);
        batch.draw(buttonStart.texture, buttonStart.screenX, buttonStart.screenY, buttonStart.size * ANDROID_WIDTH, buttonStart.size * ANDROID_HEIGHT, 0, 0, 256, 64, false, false);
        batch.draw(buttonOptions.texture, buttonOptions.screenX, buttonOptions.screenY, buttonOptions.size * ANDROID_WIDTH, buttonOptions.size * ANDROID_HEIGHT, 0, 0, 256, 64, false, false);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        ANDROID_WIDTH = Gdx.graphics.getWidth();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
