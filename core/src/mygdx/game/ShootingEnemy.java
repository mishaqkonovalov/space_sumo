package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;

public class ShootingEnemy extends Enemy {
    ShootingEnemy(float width, float height, Vector2 pos, long attackSpeed, double angularSpeed, float enginePower, float brakePower, float maxSpeed, int hp, long attackResistTime, float density, ArrayList<Texture> anim, World world) {
        super(width, height, pos, attackSpeed, angularSpeed, enginePower, brakePower, maxSpeed, hp, attackResistTime, density, anim, world);
        body.setUserData("ShootingEnemy");
    }

    void shoot(ListsOfGameObjects lists) {
        if(angleToRotate == getAngle() && TimeUtils.millis() - lastAttackTime > attackSpeed) {
            lists.addBullet(1.5f, 1.5f, new Vector2(getBodyCenter().x + MathUtils.cos(getAngle()) * (width / 2 + 1f), getBodyCenter().y + MathUtils.sin(getAngle()) * (width / 2 + 1f)), 5f, world, getAngle(), "Bullet", body);
        }
    }

    void update(Vector2 playerPos) {
        super.update();
        animStep();
        findPlayer(playerPos);
        rotate();
        move();
        damageChecker();
    }

    void damageChecker() {
        if(body.getUserData().equals("GetDamage")) {
            if(TimeUtils.millis() - lastDamageGet > attackResistTime) {
                hp--;
            }
            body.setUserData("ShootingEnemy");
        }
        if(hp <= 0) body.setUserData("Destroy");
    }
}
