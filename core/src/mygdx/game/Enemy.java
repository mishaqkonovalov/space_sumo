package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class Enemy extends Ship {

    Enemy(float width, float height, Vector2 pos, long attackSpeed, double angularSpeed, float enginePower, float brakePower, float maxSpeed, int hp, long attackResistTime, float density, ArrayList<Texture> anim, World world) {
        super(width, height, pos, attackSpeed, angularSpeed, enginePower, brakePower, maxSpeed, hp, attackResistTime, density, anim, world);
        body = createCircleBody();
        body.setUserData("Enemy");
    }

    void update() {}

    void findPlayer(Vector2 playerPos) {
        Vector2 selfPos = body.getPosition();
        double deltX = playerPos.x - selfPos.x;
        double deltY = playerPos.y - selfPos.y;
        double hypotenuse = Math.sqrt(deltX * deltX + deltY * deltY);
        double cos = deltX / hypotenuse;
        double sin = deltY / hypotenuse;
        angleToRotate = Math.atan2(sin, cos);
    }
}
