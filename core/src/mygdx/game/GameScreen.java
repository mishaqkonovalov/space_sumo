package mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;

// TODO: Пофиксить баг с отталкиванием Listener(ПЕРЕПИСАТЬ ПОЛНОСТЬЮ ContactListener)
// TODO: Сделать комнаты круглыми
// TODO: Переписать камеру(как-нибудь потом)
// TODO: Добавить сохранение размера кнопок при изменении разрешения экрана
// TODO: BitMapFont отображение текста
// TODO: Переход из экрана игры в экран меню
// TODO: Разные виды оружия (резиновые пули, самонаводящиеся ракеты)
// TODO: Добавить сохранение игры

public class GameScreen implements Screen {
    public static final int ANDROID_HEIGHT = Gdx.graphics.getHeight();
    public static final int SCR_HEIGHT = 80;
    public static final float SCREEN_SCALE = ANDROID_HEIGHT / SCR_HEIGHT;
    public int ANDROID_WIDTH = Gdx.graphics.getWidth();
    public int SCR_WIDTH = (int) (ANDROID_WIDTH / SCREEN_SCALE);

    float touchpadBaseSize = 0.3f;
    float touchpadPointSize = 0.1f;
    float buttonSize = 0.15f;
    float hpCellSize = 0.03f;
    float buttonGameOptionsSize = 0.12f;
    float buttonIndent = 0.035f;
    float descriptionSize = 0.1f;
    float descriptionYPos = 0.04f;
    float hpCellYPos = 0.935f;
    Vector2 touchpadBasePos = new Vector2(0.045f, 0.09f);
    Vector2 buttonStopPos = new Vector2(0.6f, 0.09f);
//    Vector2 buttonGameOptionsPos = new Vector2(0.84f, 0.84f);

    long lastGameOverTime;
    long gameOverTimeDelay = 400;

    Game game;

    Button touchpadBase;
    Button touchpadPoint;
    Button buttonStop;
    Button buttonMove;
    Button buttonShoot;
//    Button buttonGameOptions;
    SpriteBatch batch;
    SpriteBatch interfaceBatch;
    Camera camera;
    Box2DDebugRenderer debugRenderer;
    Vector2 touch;

    Texture background;
    Texture touchpadBaseTexture;
    Texture touchpadPointTexture;
    Texture buttonStopTexture;
    Texture buttonMoveTexture;
    Texture buttonShootTexture;
    Texture buttonGameOptionsTexture;
    Texture chestTexture;
    Texture shopTexture;
    Texture hpFullCellTexture;
    Texture hpEmptyCellTexture;
    Texture interactButtonTexture;
    Texture chestDescriptionTexture;
    Texture shopDescriptionTexture;
    Texture gameOverScreenTexture;

    World world;
    Listener listener;
    ListsOfGameObjects listGO;
    LevelMaker lvlMaker;
    RoomController roomController;
    Hero hero;
    ArrayList<Texture> heroTextures = new ArrayList<Texture>();
    ArrayList<Texture> enemyTextures = new ArrayList<>();
    ArrayList<Texture> wallTextures = new ArrayList<Texture>();
    ArrayList<Texture> cornerTextures = new ArrayList<>();
    ArrayList<Texture> doorTextures = new ArrayList<>();
    ArrayList<Texture> exitDoorTextures = new ArrayList<>();
    ArrayList<Texture> bulletTextures = new ArrayList<>();

    boolean hasIntract = false;
    boolean hasGameOver = false;

    public GameScreen(Game game) {
        this.game = game;
        Box2D.init();
        world = new World(new Vector2(0, 0), true);
        listener = new Listener();
        camera = new Camera(SCR_WIDTH, SCR_HEIGHT);
        debugRenderer = new Box2DDebugRenderer();
        touch = new Vector2();
        batch = new SpriteBatch();
        interfaceBatch = new SpriteBatch();

        background = new Texture("space.png");
        touchpadBaseTexture = new Texture("interface/touchpadBase.png");
        touchpadPointTexture = new Texture("interface/touchpadPoint.png");
        buttonStopTexture = new Texture("interface/buttonStop.png");
        buttonMoveTexture = new Texture("interface/buttonMove.png");
        buttonShootTexture = new Texture("interface/buttonShoot.png");
        buttonGameOptionsTexture = new Texture("interface/GameOptions.png");
        heroTextures.add(new Texture("Main_hero/heroGo1.png"));
        heroTextures.add(new Texture("Main_hero/heroGo2.png"));
        enemyTextures.add(new Texture("Main_hero/enemyCharged.png"));
        enemyTextures.add(new Texture("Main_hero/enemyDischarged.png"));
        wallTextures.add(new Texture("Wall/Wall1.png"));
        wallTextures.add(new Texture("Wall/Wall2.png"));
        cornerTextures.add(new Texture("Wall/roomCorner1.png"));
        cornerTextures.add(new Texture("Wall/roomCorner2.png"));
        doorTextures.add(new Texture("Wall/Door1.png"));
        doorTextures.add(new Texture("Wall/Door2.png"));
        exitDoorTextures.add(new Texture("ExitDoor1.png"));
        exitDoorTextures.add(new Texture("ExitDoor2.png"));
        bulletTextures.add(new Texture("Bullet.png"));
        chestTexture = new Texture("Chest.png");
        shopTexture = new Texture("Shop.png");
        hpFullCellTexture = new Texture("HeartFull.png");
        hpEmptyCellTexture = new Texture("HeartEmpty.png");
        interactButtonTexture = new Texture("InteractButton.png");
        chestDescriptionTexture = new Texture("ChestDesсription.png");
        shopDescriptionTexture = new Texture("ShopDescription.png");
        gameOverScreenTexture = new Texture("GameOver.png");

        listGO = new ListsOfGameObjects(wallTextures, cornerTextures, doorTextures, exitDoorTextures, enemyTextures, bulletTextures, chestTexture, shopTexture);

        touchpadBase = new Button(touchpadBasePos.x * ANDROID_WIDTH, touchpadBasePos.y * ANDROID_HEIGHT, touchpadBaseSize * ANDROID_HEIGHT, touchpadBaseTexture);
        touchpadPoint = new Button(touchpadBase.screenX + (touchpadBase.size - touchpadPointSize * ANDROID_HEIGHT) / 2, touchpadBase.screenY + (touchpadBase.size - touchpadPointSize * ANDROID_HEIGHT) / 2, touchpadPointSize * ANDROID_HEIGHT, touchpadPointTexture);
        buttonStop = new Button(buttonStopPos.x * ANDROID_WIDTH, buttonStopPos.y * ANDROID_HEIGHT, buttonSize * ANDROID_HEIGHT, buttonStopTexture);
        buttonMove = new Button(buttonStop.screenX + buttonStop.size + buttonIndent * ANDROID_WIDTH, buttonStop.screenY, buttonSize * ANDROID_HEIGHT, buttonMoveTexture);
        buttonShoot = new Button(buttonMove.screenX + buttonMove.size + buttonIndent * ANDROID_WIDTH, buttonMove.screenY, buttonSize * ANDROID_HEIGHT, buttonShootTexture);
//        buttonGameOptions = new Button(buttonGameOptionsPos.x * ANDROID_WIDTH, buttonGameOptionsPos.y * ANDROID_HEIGHT, buttonGameOptionsSize * ANDROID_HEIGHT, buttonGameOptionsTexture);

        hero = new Hero(5, 5, new Vector2(100, 100), 400, 0.1, 0.46f, 0.03f, 1f, 10, 500, 1f, heroTextures, world, lvlMaker.maxRooms);

        lvlMaker = new LevelMaker(5, 8, 140, 140, 210, 210, 1, 1);
        lvlMaker.makeLevel();
        roomController = new RoomController();
        roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(0), hero);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        if(hasGameOver) {
            gameOver();
            return;
        }

        touchpadPoint.updateScreenPos(touchpadBase.screenX + (touchpadBase.size - touchpadPointSize * ANDROID_HEIGHT) / 2, touchpadBase.screenY + (touchpadBase.size - touchpadPointSize * ANDROID_HEIGHT) / 2);
        buttonStop.isTouched = false;
        buttonMove.isTouched = false;
        buttonShoot.isTouched = false;
        hasIntract = false;

        for (int i = 0; i < 5; i++) {
            if(!Gdx.input.isTouched(i)) continue;
            touch.set(Gdx.input.getX(i), ANDROID_HEIGHT - Gdx.input.getY(i));
            if(touch.x < ANDROID_WIDTH / 2 && touch.y < ANDROID_HEIGHT / 3 * 2.2) {
                float deltX = touch.x - (touchpadPoint.screenX + touchpadPoint.size / 2);
                float deltY = touch.y - (touchpadPoint.screenY + touchpadPoint.size / 2);
                double hypotenuse = Math.sqrt(deltX * deltX + deltY * deltY);
                double cos = (float) (deltX / hypotenuse);
                double sin = (float) (deltY / hypotenuse);
                hero.angleToRotate = Math.atan2(sin, cos);
                if (hypotenuse > touchpadBase.size / 2)
                    hypotenuse = touchpadBase.size / 2;
                touchpadPoint.updateScreenPos(touchpadBase.screenX + (touchpadBase.size - touchpadPoint.size) / 2 + (float) (cos * hypotenuse), touchpadBase.screenY + (touchpadBase.size - touchpadPoint.size) / 2 + (float) (sin * hypotenuse));
            }
            if(!buttonStop.isTouched && touch.x > buttonStop.screenX && touch.y > buttonStop.screenY && touch.x < buttonStop.screenX + buttonStop.size && touch.y < buttonStop.screenY + buttonStop.size) {
                buttonStop.isTouched = true;
            }
            if(!buttonMove.isTouched && touch.x > buttonMove.screenX && touch.y > buttonMove.screenY && touch.x < buttonMove.screenX + buttonMove.size && touch.y < buttonMove.screenY + buttonMove.size) {
                buttonMove.isTouched = true;
            }
            if(!buttonShoot.isTouched && touch.x > buttonShoot.screenX && touch.y > buttonShoot.screenY && touch.x < buttonShoot.screenX + buttonShoot.size && touch.y < buttonShoot.screenY + buttonShoot.size) {
                buttonShoot.isTouched = true;
            }
//            if(touch.x > buttonGameOptions.screenX && touch.y > buttonGameOptions.screenY && touch.x < buttonGameOptions.screenX + buttonGameOptions.size && touch.y < buttonGameOptions.screenY + buttonGameOptions.size) {
//                if(buttonGameOptions.isTouched) buttonGameOptions.isTouched = false;
//                else buttonGameOptions.isTouched = true;
//            }
        }

//        if(buttonGameOptions.isTouched) {
//            game.setScreen(new MainMenuScreen(game));
//            buttonGameOptions.isTouched = false;
//            dispose();
//            return;
//        }

        listener.bodiesContact(world);

//      Не круто выглядит
        if(hero.body.getUserData().equals("LeftDoor")) {
            roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(lvlMaker.roomsMass[hero.roomY][hero.roomX - 1]), hero);
            hero.roomX -= 1;
        } else if(hero.body.getUserData().equals("RightDoor")) {
            roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(lvlMaker.roomsMass[hero.roomY][hero.roomX + 1]), hero);
            hero.roomX += 1;
        } else if(hero.body.getUserData().equals("UpDoor")) {
            roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(lvlMaker.roomsMass[hero.roomY - 1][hero.roomX]), hero);
            hero.roomY -= 1;
        } else if(hero.body.getUserData().equals("DownDoor")) {
            roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(lvlMaker.roomsMass[hero.roomY + 1][hero.roomX]), hero);
            hero.roomY += 1;
        }

//      Осуждаю
        if(roomController.room.roomState == 2) {
            if(listGO.chest.hasMeet(hero.getBodyCenter())) {
                hasIntract = true;
            }
        } else if(roomController.room.roomState == 3) {
            if(listGO.shop.hasMeet(hero.getBodyCenter())) {
                hasIntract = true;
            }
        } else if(roomController.room.roomState == 4) {
            if(listGO.exitDoor.hasMeet(hero.getBodyCenter())) {
                hasIntract = true;
            }
            listGO.exitDoor.animStep();
        }

        if(buttonMove.isTouched) hero.move();
        if(buttonStop.isTouched) hero.stop();
        else hero.body.setLinearDamping(0);
        if(buttonShoot.isTouched && !hasIntract) hero.shoot(listGO);
        else if(buttonShoot.isTouched && hasIntract) {
            if(roomController.room.roomState == 2) {
                hero.bulletDensity += 10f;
                roomController.room.roomState = 1;
            } else if(roomController.room.roomState == 3) {
                hero.upHp(3);
                roomController.room.roomState = 1;
            } else if(roomController.room.roomState == 4) {
                lvlMaker.makeLevel();
                hero.roomX = lvlMaker.maxRooms;
                hero.roomY = lvlMaker.maxRooms;
                roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(lvlMaker.roomsMass[hero.roomY][hero.roomX]), hero);
            }
        }

        hero.update();

        for(int i = 0; i < listGO.enemiesArr.size(); i++) {
            listGO.enemiesArr.get(i).update(hero.getBodyCenter());
        }

        roomController.updateRoom(listGO, world);

        for(int i = 0; i < listGO.bulletsArr.size(); i++) {
            listGO.bulletsArr.get(i).animStep();
        }
        for(int i = 0; i < listGO.wallsArr.size(); i++) {
            listGO.wallsArr.get(i).animStep();
        }
        for(int i = 0; i < listGO.doorsArr.size(); i++) {
            listGO.doorsArr.get(i).animStep();
        }

        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update(hero.getBodyCenter());

        world.step(delta, 4, 4);

        batch.setProjectionMatrix(camera.combined());
        batch.begin();
        batch.draw(background, camera.camera.position.x - SCR_WIDTH / 2, camera.camera.position.y - SCR_HEIGHT / 2, SCR_WIDTH, SCR_HEIGHT, 0, 0, 1920, 1080, false, false);

        for(int i = 0; i < listGO.wallsArr.size(); i++) {
            Wall wall = listGO.wallsArr.get(i);
            batch.draw(wall.getAnimPic(), wall.getPos().x, wall.getPos().y,
                    0, 0,
                    wall.width, wall.height,
                    1, 1, wall.getAngle()*MathUtils.radiansToDegrees,
                    0, 0, 256, 256, false, false);
        }
        for(int i = 0; i < listGO.doorsArr.size(); i++) {
            Door door = listGO.doorsArr.get(i);
            if(!door.body.isActive()) break;
            batch.draw(door.getAnimPic(), door.getPos().x, door.getPos().y,
                    0, 0,
                    door.width, door.height,
                    1, 1, door.getAngle()*MathUtils.radiansToDegrees,
                    0, 0, 64, 160, false, false);
        }
        if(roomController.room.roomState == 2) {
            Chest chest = listGO.chest;
            batch.draw(chest.texture, chest.x, chest.y, 0, 0, chest.width, chest.height, 1, 1, 0, 0, 0, 256, 256, false, false);
        } else if(roomController.room.roomState == 3) {
            Shop shop = listGO.shop;
            batch.draw(shop.texture, shop.x, shop.y, 0, 0, shop.width, shop.height, 1, 1, 0, 0, 0, 256, 256, false, false);
        } else if(roomController.room.roomState == 4) {
            ExitDoor exitDoor = listGO.exitDoor;
            batch.draw(exitDoor.getAnimPic(), exitDoor.getPos().x, exitDoor.getPos().y, 0, 0, exitDoor.width, exitDoor.height, 1, 1, 0, 0, 0, 256, 256, false, false);
        }

        batch.draw(hero.getAnimPic(), hero.getPos().x, hero.getPos().y,
                0, 0,
                hero.width, hero.height,
                1, 1, hero.getAngle()*MathUtils.radiansToDegrees,
                0, 0, 384, 256, false, false);
        for(int i = 0; i < listGO.enemiesArr.size(); i++) {
            ImpulseEnemy enemy = listGO.enemiesArr.get(i);
            batch.draw(enemy.getAnimPic(), enemy.getPos().x, enemy.getPos().y,
                    0, 0,
                    enemy.width, enemy.height,
                    1, 1, enemy.getAngle()*MathUtils.radiansToDegrees,
                    0, 0, 256, 256, false, false);
        }

        for(int i = 0; i < listGO.bulletsArr.size(); i++) {
            Bullet bullet = listGO.bulletsArr.get(i);
            batch.draw(bullet.getAnimPic(), bullet.getPos().x, bullet.getPos().y, 0, 0, bullet.width, bullet.height, 1, 1,
                    bullet.getAngle()*MathUtils.radiansToDegrees, 0, 0, 256, 256, false, false);
        }


        batch.end();

        interfaceBatch.begin();
        interfaceBatch.draw(touchpadBase.texture, touchpadBase.screenX, touchpadBase.screenY, touchpadBase.size, touchpadBase.size, 0, 0, 256, 256, false, false);
        interfaceBatch.draw(touchpadPoint.texture, touchpadPoint.screenX, touchpadPoint.screenY, touchpadPoint.size, touchpadPoint.size, 0, 0, 256, 256, false, false);
        interfaceBatch.draw(buttonStop.texture, buttonStop.screenX, buttonStop.screenY, buttonStop.size, buttonStop.size, 0, 0, 256, 256, false, false);
        interfaceBatch.draw(buttonMove.texture, buttonMove.screenX, buttonMove.screenY, buttonMove.size, buttonMove.size, 0, 0, 256, 256, false, false);
        if(hasIntract) {
            interfaceBatch.draw(interactButtonTexture, buttonShoot.screenX, buttonShoot.screenY, buttonShoot.size, buttonShoot.size, 0, 0, 256, 256, false, false);
        }else interfaceBatch.draw(buttonShoot.texture, buttonShoot.screenX, buttonShoot.screenY, buttonShoot.size, buttonShoot.size, 0, 0, 256, 256, false, false);
//        interfaceBatch.draw(buttonGameOptions.texture, buttonGameOptions.screenX, buttonGameOptions.screenY, buttonShoot.size, buttonShoot.size, 0, 0, 256, 256, false, false);
        for(int i = 0; i < hero.maxHp; i++) {
            Texture hpCellTexture;
            if(i < hero.hp) hpCellTexture = hpFullCellTexture;
            else  hpCellTexture = hpEmptyCellTexture;
            interfaceBatch.draw(hpCellTexture, (ANDROID_WIDTH - hero.maxHp * hpCellSize * ANDROID_HEIGHT * 2) / 2 + i * hpCellSize * 2 * ANDROID_HEIGHT, ANDROID_HEIGHT * hpCellYPos, hpCellSize * 2 * ANDROID_HEIGHT, hpCellSize * ANDROID_HEIGHT, 0, 0, 128, 64, false, false);
            if(hasIntract) {
                if(roomController.room.roomState == 2) {
                    interfaceBatch.draw(chestDescriptionTexture, (ANDROID_WIDTH - descriptionSize * 2) / 2 - 0.1f * ANDROID_WIDTH, descriptionYPos * ANDROID_HEIGHT, descriptionSize * 2 * ANDROID_HEIGHT, descriptionSize * ANDROID_HEIGHT, 0, 0, 128, 64, false, false);
                } else if(roomController.room.roomState == 3) {
                    interfaceBatch.draw(shopDescriptionTexture, (ANDROID_WIDTH - descriptionSize * 2) / 2 - 0.1f * ANDROID_WIDTH, descriptionYPos * ANDROID_HEIGHT, descriptionSize * 2 * ANDROID_HEIGHT, descriptionSize * ANDROID_HEIGHT, 0, 0, 128, 64, false, false);
                }
            }
        }
        interfaceBatch.end();

        destroyObj();
//        debugRenderer.render(world, camera.combined());
    }

    @Override
    public void resize(int width, int height) {
        ANDROID_WIDTH = Gdx.graphics.getWidth();
        SCR_WIDTH = ANDROID_WIDTH * SCR_HEIGHT / ANDROID_HEIGHT;
        camera.camera.setToOrtho(false, SCR_WIDTH, SCR_HEIGHT);
        touchpadBase.updateScreenPos(touchpadBasePos.x * ANDROID_WIDTH, touchpadBasePos.y * ANDROID_HEIGHT, touchpadBaseSize * ANDROID_HEIGHT);
        touchpadPoint.updateScreenPos(touchpadBase.screenX + (touchpadBase.size - touchpadPointSize * ANDROID_HEIGHT) / 2, touchpadBase.screenY + (touchpadBase.size - touchpadPointSize * ANDROID_HEIGHT) / 2, touchpadPointSize * ANDROID_HEIGHT);
        buttonStop.updateScreenPos(buttonStopPos.x * ANDROID_WIDTH, buttonStopPos.y * ANDROID_HEIGHT, buttonSize * ANDROID_HEIGHT);
        buttonMove.updateScreenPos(buttonStop.screenX + buttonStop.size + buttonIndent * ANDROID_WIDTH, buttonStop.screenY, buttonSize * ANDROID_HEIGHT);
        buttonShoot.updateScreenPos(buttonMove.screenX + buttonMove.size + buttonIndent * ANDROID_WIDTH, buttonMove.screenY, buttonSize * ANDROID_HEIGHT);
//        buttonGameOptions.updateScreenPos(buttonGameOptionsPos.x * ANDROID_WIDTH, buttonGameOptionsPos.y * ANDROID_HEIGHT, buttonGameOptionsSize * ANDROID_HEIGHT);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        debugRenderer.dispose();
        background.dispose();
        world.dispose();
        for(int i =0; i < heroTextures.size(); i++) heroTextures.get(i).dispose();
    }

//    void drawObjectsList(ArrayList<GameObject> list) {
//        for(int i = 0; i < list.size(); i++) {
//            GameObject object = list.get(i);
//            batch.draw();
//        }
//    }

    void gameOver() {
        interfaceBatch.begin();
        interfaceBatch.draw(gameOverScreenTexture, 0, 0, ANDROID_WIDTH, ANDROID_HEIGHT, 0, 0, 1920, 1080, false, false);
        interfaceBatch.end();
        if(TimeUtils.millis() - lastGameOverTime > gameOverTimeDelay && Gdx.input.isTouched()) {
            hasGameOver = false;
            lvlMaker.makeLevel();
            hero.roomX = lvlMaker.maxRooms;
            hero.roomY = lvlMaker.maxRooms;
            roomController.changeRoom(listGO, world, lvlMaker.roomsInfo.get(lvlMaker.roomsMass[hero.roomY][hero.roomX]), hero);
            hero.body.setUserData("Player");
            hero.hp = hero.maxHp;
        }
    }

    void destroyObj() {
        for(int i = 0; i < listGO.bulletsArr.size(); i++) {
            if(listGO.bulletsArr.get(i).body.getUserData().equals("Destroy") || listGO.bulletsArr.get(i).body.getUserData().equals("GetDamage")) {
                world.destroyBody(listGO.bulletsArr.get(i).body);
                listGO.bulletsArr.remove(i);
                i--;
            }
        }
        for(int i = 0; i < listGO.enemiesArr.size(); i++) {
            if(listGO.enemiesArr.get(i).body.getUserData().equals("Destroy")) {
                world.destroyBody(listGO.enemiesArr.get(i).body);
                listGO.enemiesArr.remove(i);
                i--;
            }
        }
        if (hero.body.getUserData().equals("Destroy")) {
            hasGameOver = true;
            lastGameOverTime = TimeUtils.millis();
        }
    }
}
