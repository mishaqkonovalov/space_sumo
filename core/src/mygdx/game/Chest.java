package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class Chest {
    float x, y;
    float width, height;
    Texture texture;

    public Chest(float x, float y, float width, float height, Texture texture) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.texture = texture;
    }
    boolean hasMeet(Vector2 pos) {
        if(pos.x > x && pos.x < x + width && pos.y > y && pos.y < y + height) {
            return true;
        }
        return false;
    }
}
