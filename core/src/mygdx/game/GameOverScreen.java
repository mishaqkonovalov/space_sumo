package mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameOverScreen implements Screen {
    public static final int ANDROID_HEIGHT = Gdx.graphics.getHeight();
    public int ANDROID_WIDTH = Gdx.graphics.getWidth();

    Game game;
    SpriteBatch batch;

    Texture buttonContinueTexture;
    Texture buttonStartTexture;
    Texture buttonOptionsTexture;

    GameOverScreen(Game game) {
        this.game = game;
        batch = new SpriteBatch();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        if(Gdx.input.isTouched()) {
            game.setScreen(new MainMenuScreen(game));
            dispose();
        }
        System.out.println("GameOver");
    }

    @Override
    public void resize(int width, int height) {
        ANDROID_WIDTH = Gdx.graphics.getWidth();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
