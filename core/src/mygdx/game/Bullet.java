package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class Bullet extends DynamicObject {
    int bulletSpeed;
    boolean remove = false;

    Bullet(float width, float height, Vector2 pos, float density, ArrayList<Texture> anim, World world, String type) {
        super(width, height, pos, density, anim, world);
        body = createCircleBody();
        body.setUserData(type);
    }
}
