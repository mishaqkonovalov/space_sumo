package mygdx.game;

import com.badlogic.gdx.graphics.Texture;

public class Button {
    float screenX, screenY;
    float size;
    boolean isTouched = false;
    Texture texture;

    public Button(float screenX, float screenY, float size, Texture texture) {
        this.screenX = screenX;
        this.screenY = screenY;
        this.size = size;
        this.texture = texture;
    }

    void updateScreenPos(float screenX, float screenY) {
        this.screenX = screenX;
        this.screenY = screenY;
    }
    void updateScreenPos(float screenX, float screenY, float size) {
        this.screenX = screenX;
        this.screenY = screenY;
        this.size = size;
    }
}
