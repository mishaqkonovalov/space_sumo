package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class ListsOfGameObjects {
    ArrayList<Wall> wallsArr;
    ArrayList<Door> doorsArr;
    ArrayList<ImpulseEnemy> enemiesArr;
    ArrayList<Bullet> bulletsArr;
    Chest chest;
    Shop shop;
    ExitDoor exitDoor;

    ArrayList<Texture> wallTextures;
    ArrayList<Texture> cornerTextures;
    ArrayList<Texture> doorTextures;
    ArrayList<Texture> exitDoorTextures;
    ArrayList<Texture> enemyTextures;
    ArrayList<Texture> bulletTextures;
    Texture chestTexture;
    Texture shopTexture;
    ListsOfGameObjects(ArrayList<Texture> wallTextures, ArrayList<Texture> cornerTextures, ArrayList<Texture> doorTextures, ArrayList<Texture> exitDoorTextures, ArrayList<Texture> enemyTextures, ArrayList<Texture> bulletTexture, Texture chestTexture, Texture shopTexture) {
        wallsArr = new ArrayList<>();
        doorsArr = new ArrayList<>();
        enemiesArr = new ArrayList<>();
        bulletsArr = new ArrayList<>();

        this.wallTextures = wallTextures;
        this.cornerTextures = cornerTextures;
        this.doorTextures = doorTextures;
        this.exitDoorTextures = exitDoorTextures;
        this.enemyTextures = enemyTextures;
        this.bulletTextures = bulletTexture;
        this.chestTexture = chestTexture;
        this.shopTexture = shopTexture;
    }
    void addWall(float width, float height, Vector2 pos, float angle, World world) {
        wallsArr.add(new Wall(width, height, pos, angle, wallTextures, world));
    }
    void addCorner(float width, float height, Vector2 pos, float angle, World world) {
        wallsArr.add(new Wall(width, height, pos, angle, cornerTextures, world));
    }
    void addDoor(float width, float height, Vector2 pos, float angle, World world, String data) {
        doorsArr.add(new Door(width, height, pos, angle, doorTextures, world, data));
    }
    void addChest(float x, float y, float width, float height) {
        chest = new Chest(x, y, width, height, chestTexture);
    }
    void addShop(float x, float y, float width, float height) {
        shop = new Shop(x, y, width, height, shopTexture);
    }
    void addExitDoor(float x, float y, float width, float height, World world) {
        exitDoor = new ExitDoor(width, height, new Vector2(x, y), 0, exitDoorTextures, world);
    }
    void addBullet(float width, float height, Vector2 pos, float density, World world, float angle, String type, Body body) {
        Bullet bullet = new Bullet(width, height, pos, density, bulletTextures, world, type);
        bullet.body.setLinearVelocity(body.getLinearVelocity());
        bullet.body.setLinearVelocity(new Vector2(MathUtils.cos(angle) * 50, MathUtils.sin(angle) * 50));
        bulletsArr.add(bullet);
    }
    void addImpulseEnemy(int x, int y, World world) {
        enemiesArr.add(new ImpulseEnemy(8, 8, new Vector2(x, y), 5000, 0.1, 0.6f, 0.025f,0.5f, 3, 400, 1f, enemyTextures, world));
    }
//    void addShootingEnemy() {
//        enemiesArr.add(new ShootingEnemy());
//    }
}
