package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;

public class Hero extends Ship {

    int roomX;
    int roomY;
    float bulletDensity = 20f;
    Hero(float width, float height, Vector2 pos, long attackSpeed, double angularSpeed, float enginePower, float brakePower, float maxSpeed, int hp, long attackResistTime, float density, ArrayList<Texture> anim, World world, int maxRooms) {
        super(width, height, pos, attackSpeed, angularSpeed, enginePower, brakePower, maxSpeed, hp, attackResistTime, density, anim, world);
        body = createCircleBody();
        body.setUserData("Player");
        roomX = maxRooms;
        roomY = maxRooms;
    }

    void update() {
        animStep();
        rotate();
        damageChecker();
    }

    void shoot(ListsOfGameObjects list) {
        if(!(TimeUtils.millis() - lastAttackTime > attackSpeed)) return;
        lastAttackTime = TimeUtils.millis();
        list.addBullet(1.5f, 1.5f, new Vector2(getBodyCenter().x + MathUtils.cos(getAngle()) * (width / 2 + 1f), getBodyCenter().y + MathUtils.sin(getAngle()) * (width / 2 + 1f)), bulletDensity, world, getAngle(), "PlayerBullet", body);
    }

    void damageChecker() {
        if(body.getUserData().equals("GetDamage")) {
            if(TimeUtils.millis() - lastDamageGet > attackResistTime) {
                hp--;
                lastDamageGet = TimeUtils.millis();
            }
            body.setUserData("Player");
        }
        if(hp <= 0) body.setUserData("Destroy");
    }
    void upHp(int dt) {
        if(hp + dt > maxHp) hp = maxHp;
        else hp += dt;
    }
}