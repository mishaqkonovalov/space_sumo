package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import java.awt.geom.RectangularShape;
import java.time.temporal.ValueRange;
import java.util.ArrayList;

public class GameObject {
    BodyDef bodyDef;
    Body body;
    ArrayList<Texture> anim;
    float width, height;
    float density;
    int animIndex = 0;
    long lastTimeAnim;
    long nextAnimPicTime = 400;
    Vector2 pos;
    World world;
    GameObject(float width, float height, Vector2 pos, ArrayList<Texture> anim, World world) {
        this.width = width;
        this.height = height;
        this.pos = pos;
        bodyDef = new BodyDef();
        bodyDef.position.set(pos);
        this.anim = anim;
        this.world = world;
    }
    protected Body createCircleBody() {
        Body body = world.createBody(bodyDef);
        CircleShape circle = new CircleShape();
        circle.setPosition(new Vector2(width / 2, height / 2));
        circle.setRadius((width) / 2);
        Fixture fixture = body.createFixture(circle, density);
        circle.dispose();
        return body;
    }
    protected Body createPolygonBody() {
        Vector2 pos = new Vector2(width / 2, height / 2);
        Body body = world.createBody(bodyDef);
        PolygonShape polygon = new PolygonShape();
        polygon.setAsBox(width / 2, height / 2, pos, 0);
        Fixture fixture = body.createFixture(polygon, density);
        polygon.dispose();
        return body;
    }
    void animStep() {
        if(TimeUtils.millis() - lastTimeAnim >= nextAnimPicTime) {
            lastTimeAnim = TimeUtils.millis();
            animIndex++;
            animIndex %= anim.size();
        }
    }
    Texture getAnimPic() {
        return anim.get(animIndex);
    }
    float getAngle() {
        return body.getAngle();
    }
    Vector2 getPos() {
        return body.getPosition();
    }
    Vector2 getBodyCenter() {
        return body.getWorldCenter();
    }
}
