package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;

public class ImpulseEnemy extends Enemy {
    boolean hasAttackReady = true;
    ImpulseEnemy(float width, float height, Vector2 pos, long attackSpeed, double angularSpeed, float enginePower, float brakePower, float maxSpeed, int hp, long attackResistTime, float density, ArrayList<Texture> anim, World world) {
        super(width, height, pos, attackSpeed, angularSpeed, enginePower, brakePower, maxSpeed, hp, attackResistTime, density, anim, world);
        body.setUserData("Impulse");
    }

    void update(Vector2 playerPos) {
        animStep();
        findPlayer(playerPos);
        rotate();
        move();
        rechargeImpulse();
        damageChecker();
    }

    void rechargeImpulse() {
        if(body.getUserData().equals("Impulse")) lastAttackTime = TimeUtils.millis();
        if(body.getUserData().equals("Recharging")) {
            hasAttackReady = false;
            if(TimeUtils.millis() - lastAttackTime > attackSpeed) {
                body.setUserData("Impulse");
                hasAttackReady = true;
            }
        }
    }

    void damageChecker() {
        if(body.getUserData().equals("GetDamage")) {
            if(TimeUtils.millis() - lastDamageGet > attackResistTime) {
                hp--;
                lastDamageGet = TimeUtils.millis();
            }
            if(hasAttackReady) body.setUserData("Impulse");
            else body.setUserData("Recharging");
        }
        if(hp <= 0) body.setUserData("Destroy");
    }
}
