package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class LevelMaker {

    int[][] roomsMass;
    ArrayList<Room> roomsInfo;

    int minRooms;
    int maxRooms;
    int minWidth;
    int minHeight;
    int maxWidth;
    int maxHeight;
    int roomsWithChest;
    int roomsWithShop;

    LevelMaker(int minRooms, int maxRooms, int minWidth, int minHeight, int maxWidth, int maxHeight, int roomsWithChest, int roomsWithShop) {
        roomsInfo = new ArrayList<>();
        this.minRooms = minRooms;
        this.maxRooms = maxRooms;
        this.minWidth = minWidth;
        this.maxWidth = maxWidth;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        this.roomsWithChest = roomsWithChest;
        this.roomsWithShop = roomsWithShop;
    }

    void makeLevel() {
        int roomsCount = MathUtils.random(minRooms, maxRooms);
        int lastRoomId = 0;
        int nowRoomId = 0;
        roomsMass = new int[maxRooms * 2][maxRooms * 2];
        roomsInfo.clear();
        for(int i = 0; i < maxRooms * 2; i++) {
            for(int j = 0; j < maxRooms * 2; j++) {
                roomsMass[i][j] = -1;
            }
        }
        roomsMass[maxRooms][maxRooms] = 0;
        createRoom();
        while(lastRoomId < roomsCount) {
            int x = -1;
            int y = -1;
            if(nowRoomId >= roomsInfo.size()) {
                int a = maxRooms;
                int b = maxRooms;
                while(roomsMass[a][b] != -1) {
                    b--;
                }
                lastRoomId++;
                createRoom();
                roomsInfo.get(lastRoomId).paths[1] = true;
                roomsInfo.get(roomsMass[a][b + 1]).paths[0] = true;
            }
            for(int i = 0; i < maxRooms * 2; i++) {
                for(int j = 0; j < maxRooms * 2; j++) {
                    if(roomsMass[i][j] == nowRoomId) {
                        y = i;
                        x = j;
                        break;
                    }
                }
                if(x != -1) break;
            }

            int pathsCount = 0;
            for(int i = 0; i < 4; i++) {
                if(roomsInfo.get(nowRoomId).paths[i]) pathsCount++;
            }

            for(int i = pathsCount; i < 4; i++) {
                float pathChance = 1f;
                if (i < 2) pathChance = 1f;
                else if (i == 2) pathChance = 0.5f;
                else if (i == 3) pathChance = 0.1f;

                if(MathUtils.random() < pathChance) {
                    int pathSide;
                    do {
                        pathSide = MathUtils.random(0, 3);
                    } while(roomsInfo.get(nowRoomId).paths[pathSide]);


                    if(pathSide == 0) {
                        if(roomsMass[y][x - 1] == -1) {
                            lastRoomId++;
                            roomsMass[y][x - 1] = lastRoomId;
                            createRoom();
                        }
                        roomsInfo.get(nowRoomId).paths[0] = true;
                        roomsInfo.get(roomsMass[y][x - 1]).paths[1] = true;
                    }
                    else if(pathSide == 1) {
                        if(roomsMass[y][x + 1] == -1) {
                            lastRoomId++;
                            roomsMass[y][x + 1] = lastRoomId;
                            createRoom();
                        }
                        roomsInfo.get(nowRoomId).paths[1] = true;
                        roomsInfo.get(roomsMass[y][x + 1]).paths[0] = true;
                    } else if(pathSide == 2) {
                        if(roomsMass[y - 1][x] == -1) {
                            lastRoomId++;
                            roomsMass[y - 1][x] = lastRoomId;
                            createRoom();
                        }
                        roomsInfo.get(nowRoomId).paths[2] = true;
                        roomsInfo.get(roomsMass[y - 1][x]).paths[3] = true;
                    } else if(pathSide == 3) {
                        if(roomsMass[y + 1][x] == -1) {
                            lastRoomId++;
                            roomsMass[y + 1][x] = lastRoomId;
                            createRoom();
                        }
                        roomsInfo.get(nowRoomId).paths[3] = true;
                        roomsInfo.get(roomsMass[y + 1][x]).paths[2] = true;
                    }
                } else break;
            }
            nowRoomId++;
        }


        roomsInfo.get(0).roomState = 1;
        int exitRoomId;
        do {
            exitRoomId = MathUtils.random(1, roomsInfo.size() - 1);
        } while(roomsInfo.get(exitRoomId).roomState != 0);
        roomsInfo.get(exitRoomId).roomState = 4;

        for(int i = 0; i < roomsWithChest; i++) {
            int roomWithChestId;
            do {
                roomWithChestId = MathUtils.random(1, roomsInfo.size() - 1);
            } while(roomsInfo.get(roomWithChestId).roomState != 0);
            roomsInfo.get(roomWithChestId).roomState = 2;
        }

        for(int i = 0; i < roomsWithShop; i++) {
            int roomsWithShopId;
            do {
                roomsWithShopId = MathUtils.random(1, roomsInfo.size() - 1);
            } while(roomsInfo.get(roomsWithShopId).roomState != 0);
            roomsInfo.get(roomsWithShopId).roomState = 3;
        }

//        for(int i = 0; i < roomsInfo.size(); i++) {
//            roomsInfo.get(i).roomState = 4;
//        }
    }
    private void createRoom() {
        boolean[] paths = new boolean[4];
        for(int i = 0; i < 4; i++) {
            paths[i] = false;
        }
        roomsInfo.add(new Room(paths, 0, MathUtils.random(minWidth, maxWidth), MathUtils.random(minHeight, maxHeight)));
    }
}