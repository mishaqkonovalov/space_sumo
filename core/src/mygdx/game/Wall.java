package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class Wall extends StaticObject {
    public Wall(float width, float height, Vector2 pos, float angle, ArrayList<Texture> anim, World world) {
        super(width, height, pos, angle, anim, world);
        body = createPolygonBody();
        body.setUserData("Wall");
        body.setTransform(pos, angle * MathUtils.degRad);
    }
}
