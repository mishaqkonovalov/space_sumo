package mygdx.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

public class Camera{
    OrthographicCamera camera;
    Camera(int viewportWidth, int viewportHeight) {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, viewportWidth,  viewportHeight);
    }
    void update(Vector2 pos) {
        camera.position.x = pos.x;
        camera.position.y = pos.y;
        camera.update();
    }
    Matrix4 combined() {
        return camera.combined;
    }
}
