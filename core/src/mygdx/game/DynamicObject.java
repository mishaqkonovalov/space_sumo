package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class DynamicObject extends GameObject {
    Vector2 v;
    DynamicObject(float width, float height, Vector2 pos, float density, ArrayList<Texture> anim, World world) {
        super(width, height, pos, anim, world);
        this.bodyDef.type = BodyDef.BodyType.DynamicBody;
        this.density = density;
    }
    void addForce(Vector2 force) {
        body.applyForceToCenter(force, true);
    }
}
