package mygdx.game;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class Listener {

    public void bodiesContact(World world) {
        Array<Contact> list = world.getContactList();
        for(int i = 0; i < list.size; i++) {
            Contact contact = list.get(i);
            Fixture fixtureA = contact.getFixtureA();
            Fixture fixtureB = contact.getFixtureB();
            Body body = null;
            if(fixtureA != null && fixtureB != null) {
                Object dataA = fixtureA.getBody().getUserData();
                Object dataB = fixtureB.getBody().getUserData();
                if(dataA == null || dataB == null) return;
                if(dataA.equals("PlayerBullet")) {
                    if(!dataB.equals("Player")) body = fixtureA.getBody();
                }
                if(dataB.equals("PlayerBullet")) {
                    if(!dataA.equals("Player")) body = fixtureB.getBody();
                }
//
                if(dataA.equals("Player") && dataB.equals("Impulse") || dataA.equals("Impulse") && dataB.equals("Player") || dataA.equals("Impulse") && dataB.equals("PlayerBullet") || dataA.equals("PlayerBullet") && dataB.equals("Impulse") || dataA.equals("Recharging") && dataB.equals("PlayerBullet") || dataA.equals("PlayerBullet") && dataB.equals("Recharging")) {
                    Vector2 APos = fixtureA.getBody().getWorldCenter();
                    Vector2 BPos = fixtureB.getBody().getWorldCenter();
                    double angle = calcAngle(BPos.x - APos.x, BPos.y - APos.y);
                    System.out.println(angle * MathUtils.radiansToDegrees+" "+fixtureA.getBody().getUserData()+" "+fixtureB.getBody().getUserData());
                    if(!dataA.equals("PlayerBullet")) fixtureA.getBody().applyForceToCenter((float)Math.cos(angle) * fixtureB.getBody().getMass(), (float)Math.sin(angle) * fixtureB.getBody().getMass(), true);
                    if(!dataB.equals("PlayerBullet")) fixtureB.getBody().applyForceToCenter((float)-Math.cos(angle) * fixtureA.getBody().getMass(), (float)-Math.sin(angle) * fixtureA.getBody().getMass(), true);
                    if(dataA.equals("Impulse")) fixtureA.getBody().setUserData("Recharging");
                    if(dataB.equals("Impulse")) fixtureB.getBody().setUserData("Recharging");
                }

                if(dataA.equals("Wall")) {
                    fixtureB.getBody().setUserData("GetDamage");
                }
                if(dataB.equals("Wall")) {
                    fixtureA.getBody().setUserData("GetDamage");
                }

                if(dataA.equals("LeftDoor")) {
                    fixtureB.getBody().setUserData("LeftDoor");
                } else if(dataB.equals("LeftDoor")) {
                    fixtureA.getBody().setUserData("LeftDoor");
                } else if(dataA.equals("RightDoor")) {
                    fixtureB.getBody().setUserData("RightDoor");
                } else if(dataB.equals("RightDoor")) {
                    fixtureA.getBody().setUserData("RightDoor");
                } else if(dataA.equals("UpDoor")) {
                    fixtureB.getBody().setUserData("UpDoor");
                } else if(dataB.equals("UpDoor")) {
                    fixtureA.getBody().setUserData("UpDoor");
                } else if(dataA.equals("DownDoor")) {
                    fixtureB.getBody().setUserData("DownDoor");
                } else if(dataB.equals("DownDoor")) {
                    fixtureA.getBody().setUserData("DownDoor");
                }
            }


            if (body != null) {
                body.setUserData("Destroy");
            }
        }
    }
    double calcAngle(float deltX, float deltY) {
        double hypotenuse = Math.sqrt(deltX * deltX + deltY * deltY);
        float cos = (float) (deltX / hypotenuse);
        float sin = (float) (deltY / hypotenuse);
        return Math.atan2(cos, sin);
    }
}
