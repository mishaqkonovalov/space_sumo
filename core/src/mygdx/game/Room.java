package mygdx.game;

public class Room {
    // roomState: 0. Не зачищена, 1. Зачищена, 2. Есть сундук, 3. Есть магазин, 4. Выход
    boolean[] paths;
    int roomState;
    int width;
    int height;
    public Room(boolean[] paths, int roomState, int width, int height) {
        this.paths = paths; // left, right, up, down
        this.roomState = roomState;
        this.width = width;
        this.height = height;
    }
}
