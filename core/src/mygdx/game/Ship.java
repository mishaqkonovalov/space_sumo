package mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;

public class Ship extends DynamicObject {
    double angleToRotate = 0;
    long lastAttackTime = 0;
    long lastDamageGet = 0;
    long attackSpeed;
    double angularSpeed;
    float enginePower;
    float brakePower;
    float maxSpeed;
    int maxHp;
    int hp;
    long attackResistTime;

    Ship(float width, float height, Vector2 pos, long attackSpeed, double angularSpeed, float enginePower, float brakePower, float maxSpeed, int hp, long attackResistTime, float density, ArrayList<Texture> anim, World world) {
        super(width, height, pos, density, anim, world);
        this.attackSpeed = attackSpeed;
        this.angularSpeed = angularSpeed;
        this.enginePower = enginePower;
        this.brakePower = brakePower;
        this.maxSpeed = maxSpeed;
        this.density = density;
        this.maxHp = hp;
        this.hp = hp;
        this.attackResistTime = attackResistTime;
    }


    void rotate() {
        double ang = (getAngle() % (2 * Math.PI));
        if(ang < 0) ang = (2 * Math.PI + ang);
        if(angleToRotate < 0) angleToRotate = (2 * Math.PI + angleToRotate);
        if(angleToRotate == ang) return;
        if(ang > angleToRotate) {
            if(Math.abs(angleToRotate - ang) <= Math.abs(2 * Math.PI - ang + angleToRotate)) {
                if(ang - angleToRotate >= angularSpeed) body.setAngularVelocity((float) (-angularSpeed));
                else body.setAngularVelocity((float) (angleToRotate - ang));
            } else {
                if(2 * Math.PI - ang + angleToRotate >= angularSpeed) body.setAngularVelocity((float) (angularSpeed));
                else body.setAngularVelocity((float) (2 * Math.PI - ang + angleToRotate));
            }
        } else {
            if(angleToRotate - ang <= 2 * Math.PI - angleToRotate + ang) {
                if(angleToRotate - ang >= angularSpeed) body.setAngularVelocity((float) (angularSpeed));
                else body.setAngularVelocity((float) (angleToRotate - ang));
            } else {
                if(2 * Math.PI - angleToRotate + ang >= angularSpeed) body.setAngularVelocity((float) (-angularSpeed));
                else body.setAngularVelocity((float) (-(2 * Math.PI - angleToRotate + ang) % angularSpeed));
            }
        }
    }

    void move() {
        float powerX = MathUtils.cos(getAngle()) * enginePower;
        float powerY = MathUtils.sin(getAngle()) * enginePower;
        double nowX = body.getLinearVelocity().x;
        double nowY = body.getLinearVelocity().y;
        double boostX = powerX / body.getMass();
        double boostY = powerY / body.getMass();
        if(Math.abs(nowX) >= maxSpeed) {
            if(nowX > 0) {
                if(powerX < 0) addForce(new Vector2(powerX, 0));
            } else {
                if(powerX > 0) addForce(new Vector2(powerX, 0));
            }
        } else {
            if(Math.abs(nowX + boostX) > maxSpeed) {
                if(boostX < 0) addForce(new Vector2((float)(-maxSpeed - nowX) * body.getMass(), 0));
                else addForce(new Vector2((float)(maxSpeed - nowX) * body.getMass(), 0));
            } else addForce(new Vector2(powerX, 0));
        }
        if(Math.abs(nowY) >= maxSpeed) {
            if(nowY > 0) {
                if(powerY < 0) addForce(new Vector2(0, powerY));
            } else {
                if(powerY > 0) addForce(new Vector2(0, powerY));
            }
        } else {
            if(Math.abs(nowY + boostY) > maxSpeed) {
                if(boostY < 0) addForce(new Vector2(0, (float)(-maxSpeed - nowY) * body.getMass()));
                else addForce(new Vector2(0, (float)(maxSpeed - nowY) * body.getMass()));
            } else addForce(new Vector2(0, powerY));
        }
    }

    void stop() {
        body.setLinearDamping(brakePower);
    }
}
