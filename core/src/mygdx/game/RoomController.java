package mygdx.game;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class RoomController {
    int WALL_SIZE = 10;
    float DOOR_HEIGHT = 25;
    float DOOR_WIDTH = 10;
    Room room;
    int roomWidth;
    int roomHeight;
    boolean hasActivated = false;
    int wavesCount;
    int nowWave;
    RoomController() {}

    void changeRoom(ListsOfGameObjects list, World world, Room room, Hero hero) {
        this.room = room;
        this.hasActivated = false;
        if(room.roomState == 0) {
            wavesCount = MathUtils.random(1, 3);
            nowWave = 0;
        }
        for(int i = 0; i < list.wallsArr.size(); i++) {
            world.destroyBody(list.wallsArr.get(i).body);
        }
        list.wallsArr.clear();
        for(int i = 0; i < list.enemiesArr.size(); i++) {
            world.destroyBody(list.enemiesArr.get(i).body);
        }
        list.enemiesArr.clear();
        for(int i = 0; i < list.doorsArr.size(); i++) {
            world.destroyBody(list.doorsArr.get(i).body);
        }
        if(list.exitDoor != null) world.destroyBody(list.exitDoor.body);
        list.doorsArr.clear();
        list.shop = null;
        list.chest = null;
        list.exitDoor = null;

        roomWidth = room.width / WALL_SIZE;
        roomHeight = room.height / WALL_SIZE;

        list.addCorner(WALL_SIZE, WALL_SIZE, new Vector2(-WALL_SIZE, -WALL_SIZE), 0, world);
        list.addCorner(WALL_SIZE, WALL_SIZE, new Vector2(WALL_SIZE * (roomWidth + 1),  -WALL_SIZE), 90, world);
        list.addCorner(WALL_SIZE, WALL_SIZE, new Vector2(WALL_SIZE * (roomWidth + 1), WALL_SIZE * (roomHeight + 1)), 180, world);
        list.addCorner(WALL_SIZE, WALL_SIZE, new Vector2(-WALL_SIZE, WALL_SIZE * (roomHeight + 1)), 270, world);

        for(int i = 0; i < roomWidth; i++) {
            list.addWall(WALL_SIZE, WALL_SIZE, new Vector2(WALL_SIZE * i, -WALL_SIZE), 0, world);
        }
        for(int i = 0; i < roomWidth; i++) {
            list.addWall(WALL_SIZE, WALL_SIZE, new Vector2(WALL_SIZE * i, WALL_SIZE * (roomHeight)), 0, world);
        }
        for(int i = 0; i < roomHeight; i++) {
            list.addWall(WALL_SIZE, WALL_SIZE, new Vector2(0, WALL_SIZE * i), 90, world);
        }
        for(int i = 0; i < roomHeight; i++) {
            list.addWall(WALL_SIZE, WALL_SIZE, new Vector2(WALL_SIZE * (roomWidth + 1), WALL_SIZE * i), 90, world);
        }

        if(room.paths[0]) list.addDoor(DOOR_WIDTH, DOOR_HEIGHT, new Vector2(-0.8f * WALL_SIZE, (WALL_SIZE * roomHeight - DOOR_HEIGHT) / 2), 0, world, "LeftDoor");
        if(room.paths[1]) list.addDoor(DOOR_WIDTH, DOOR_HEIGHT, new Vector2((roomWidth - 0.2f) * WALL_SIZE, (WALL_SIZE * roomHeight - DOOR_HEIGHT) / 2), 0, world, "RightDoor");
        if(room.paths[2]) list.addDoor(DOOR_WIDTH, DOOR_HEIGHT, new Vector2((WALL_SIZE * roomWidth - DOOR_HEIGHT) / 2 + DOOR_HEIGHT, (roomHeight - 0.2f) * WALL_SIZE), 90, world, "UpDoor");
        if(room.paths[3]) list.addDoor(DOOR_WIDTH, DOOR_HEIGHT, new Vector2((WALL_SIZE * roomWidth - DOOR_HEIGHT) / 2 + DOOR_HEIGHT, -0.8f * WALL_SIZE), 90, world, "DownDoor");

        if(room.roomState != 0) {
            changeToClear(list);
        }
        if(room.roomState == 2) {
            list.addChest((roomWidth * WALL_SIZE - 10) / 2, (roomHeight * WALL_SIZE - 10) / 2, 10, 10);
        } else if(room.roomState == 3) {
            list.addShop((roomWidth * WALL_SIZE - 10) / 2, (roomHeight * WALL_SIZE - 10) / 2, 10, 10);
        } else if(room.roomState == 4) {
            list.addExitDoor((roomWidth * WALL_SIZE - 10) / 2, (roomHeight * WALL_SIZE - 10) / 2, 10, 10, world);
        }

        if(hero.body.getUserData().equals("LeftDoor")) {
            hero.body.setTransform(new Vector2(roomWidth * WALL_SIZE - hero.width - 3, (roomHeight * WALL_SIZE - hero.width) / 2), hero.getAngle());
            System.out.println("LeftDoor");
        }else if(hero.body.getUserData().equals("RightDoor")) {
            hero.body.setTransform(new Vector2(8, (roomHeight * WALL_SIZE - hero.width) / 2), hero.getAngle());
            System.out.println("RightDoor");
        } else if(hero.body.getUserData().equals("UpDoor")) {
            hero.body.setTransform(new Vector2((roomWidth * WALL_SIZE - hero.width) / 2 + 3, 8), hero.getAngle());
            System.out.println("UpDoor");
        } else if(hero.body.getUserData().equals("DownDoor")) {
            hero.body.setTransform(new Vector2((roomWidth * WALL_SIZE - hero.width) / 2, roomHeight * WALL_SIZE - hero.width - 3), hero.getAngle());
            System.out.println("DownDoor");
        } else {
            hero.body.setTransform(new Vector2((roomWidth * WALL_SIZE - hero.width) / 2, (roomHeight * WALL_SIZE - hero.width) / 2), hero.getAngle());
            System.out.println("Center");
        }
        hero.body.setLinearVelocity(new Vector2(0, 0));
        hero.body.setUserData("Player");

    }

    void updateRoom(ListsOfGameObjects list, World world) {
        if(room.roomState == 0) {
            if(nowWave < wavesCount || list.enemiesArr.size() != 0) {
                if(list.enemiesArr.size() == 0) {
                    spawnWave(list, world);
                    nowWave++;
                }
            } else {
                changeToClear(list);
                room.roomState = 1;
            }
        }

    }

    void changeToClear(ListsOfGameObjects lists) {
        for(int i = 0; i < lists.doorsArr.size(); i++) {
            lists.doorsArr.get(i).body.setActive(true);
        }
    }


    void spawnWave(ListsOfGameObjects list, World world) {
        int enemiesCount = MathUtils.random(1, 3);
        for(int i = 0; i < enemiesCount; i++) {
            list.addImpulseEnemy(MathUtils.random(0, roomWidth * WALL_SIZE - 8), MathUtils.random(0, roomHeight * WALL_SIZE),world);
        }
    }
}
